class Countries{
  final String country;
  final int todayCases;
  final int cases;
  final int todayDeaths;
  final int deaths ;
  final int todayRecovered;
  final int recovered;
  final String flag;
  final int active ;

  Countries({this.active,this.flag,this.country, this.cases,this.todayCases, this.deaths, this.todayDeaths, this.recovered, this.todayRecovered});

  //code below is used to let the another screen can called (httpService.dart)
  factory Countries.fromJson(Map<String,dynamic>json){
    return Countries(
      country: json['country'],
      cases: json['cases'] as int,
      todayCases: json['todayCases'] as int,
      deaths: json['deaths'] as int,
      todayDeaths: json['todayDeaths'] as int,
      recovered:json['recovered'] as int,
      todayRecovered:json['todayRecovered'] as int,
      flag:json['countryInfo']['flag'],
      active:json['active'] as int,
    );
  }
}
