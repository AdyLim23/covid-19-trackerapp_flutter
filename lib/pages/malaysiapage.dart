import 'package:flutter/material.dart';

import '../countries_model.dart';
import '../malaysia_model.dart';

class MalaysiaPage extends StatelessWidget {
  // List <Countries> countryData;
  // MalaysiaPage({Key key, this.countryData}) : super(key: key);
  Malaysia malaysia;
  MalaysiaPage({Key key, this.malaysia}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(title: Text("Malaysia"),centerTitle: true,),
      body:Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children:[
            Image.network(malaysia.flag) ,
            Container(
              margin: EdgeInsets.symmetric(vertical:50),
              child:Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("CONFIRMED: " + malaysia.cases.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.red)),
                  Text("Active: " + malaysia.active.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.blue)),
                  Text("RECOVERED: " +malaysia.recovered.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.green)),
                  Text("DEATHS: " + malaysia.deaths.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.grey)),
                ],
              )
            ),
            Container(
              child:GridView(
              shrinkWrap: true,
              //whole page is scrollable but only grid is not scrollable
              physics:NeverScrollableScrollPhysics(),
              //TWICE time of the height compare to width ,then we will get nice rectangle here
              //crossAxisCount :2 means there is 2 object/things/material in a row WHILE childAspectRatio 2 means is the ratio of the width/height size
              gridDelegate:SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,childAspectRatio: 2),
              children: [
                StatusPanel(
                  title:"Today_Cases",
                  panelColor: Colors.red[300],
                  textColor:Colors.red,
                  count:malaysia.todayCases.toString(),
                ),
                StatusPanel(
                  title:"Today_Recovered",
                  panelColor: Colors.green[300],
                  textColor:Colors.green,
                  count:malaysia.todayRecovered.toString(),
                ),
                StatusPanel(
                  title:"Today_Deaths",
                  panelColor: Colors.grey[600],
                  textColor:Colors.red,
                  count:malaysia.todayDeaths.toString(),
                ),
              ]))
            // Container(
            //   child:Row(
            //     children: [
            //       Text("Today_Cases: "+ malaysia.todayCases.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.red)),
            //        Text("Today_Recovered: "+ malaysia.todayRecovered.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.green)),
            //         Text("Today_Deaths: "+ malaysia.todayDeaths.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.grey))
            //     ],
            //   )
            // )
            ]
        )

      ));
      
  }
}

class StatusPanel extends StatelessWidget {

  final Color panelColor;
  final Color textColor;
  final String title;
  final String count;

  const StatusPanel({Key key, this.panelColor, this.textColor, this.title, this.count}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      margin:EdgeInsets.all(10),
      height:80,
      width: width/4,
      color:panelColor,
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(title,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color:Colors.white)),
          Text(count,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color:Colors.white))
        ],
      )
    );
  }
}