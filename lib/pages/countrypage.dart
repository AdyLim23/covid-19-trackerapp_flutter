import 'package:flutter/material.dart';

import '../countries_model.dart';

class CountryPage extends StatelessWidget {
  List <Countries> countryData;
  CountryPage({Key key, this.countryData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(title: Text("All Countries")),
      body:SingleChildScrollView(child:Container(
      
        child:ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount:218,
          itemBuilder: (context,index){
            return Container(
            height:130,
            margin:EdgeInsets.symmetric(horizontal:10,vertical: 10),
            decoration: BoxDecoration(color: Colors.white,
                        boxShadow:[ BoxShadow(color:Colors.grey[100],blurRadius: 10,offset: Offset(0,10),
            )]),
            child:Row(
              children: [
                Container(
                  child:Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(countryData[index].country, style:TextStyle(fontFamily: "OpenSansBold",fontWeight: FontWeight.bold)),
                      Image.network(countryData[index].flag,height:50,width:80),

                    ],
                  )
                ),
                Expanded(child:Container(
                  child:Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("CONFIRMED: " + countryData[index].cases.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.red)),
                      Text("Active: " + countryData[index].active.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.blue)),
                      Text("RECOVERED: " +countryData[index].recovered.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.green)),
                      Text("DEATHS: " + countryData[index].deaths.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",color: Colors.grey)),
                    ],
                  )
                ))
                // SizedBox(width:10),
                // SizedBox(width:10),
                // Text("Deaths:" + countryData[index].deaths.toString() ,style:TextStyle(color: Colors.red,fontFamily: "OpenSansBold")),
                // Text("Recovered:" + countryData[index].recovered.toString(),style:TextStyle(color: Colors.green,fontFamily: "OpenSansBold"))
              ],
            )
          );
        },
      )
      )));
  }
}