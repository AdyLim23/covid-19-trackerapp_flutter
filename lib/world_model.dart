class World{
  final int cases;
  final int todayCases;
  final int deaths;
  final int todayDeaths;
  final int recovered ;
  final int todayRecovered;
  final int active;

  World({this.cases, this.todayCases, this.deaths, this.todayDeaths, this.recovered, this.todayRecovered, this.active});

  //code below is used to let the another screen can called (httpService.dart)
  factory World.fromJson(Map<String,dynamic>json){
    return World(
      active:json['active'] as int,
      cases: json['cases'] as int,
      todayCases: json['todayCases'] as int,
      deaths: json['deaths'] as int,
      todayDeaths: json['todayDeaths'] as int,
      recovered:json['recovered'] as int,
      todayRecovered:json['todayRecovered'] as int,
    );
  }
}